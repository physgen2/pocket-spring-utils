package com.freesolutions.pocket.pocketspringutils.controller.advice;

import com.fasterxml.jackson.core.JsonLocation;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import com.freesolutions.pocket.pocketrestdata.dto.ApiResponse;
import com.freesolutions.pocket.pocketrestdata.dto.Reason;
import com.freesolutions.pocket.pocketrestdata.exception.ResponseException;
import com.freesolutions.pocket.pocketrestdata.exception.RestExchangeException;
import com.freesolutions.pocket.pocketrestdata.value.KeyAware;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.method.annotation.MethodArgumentConversionNotSupportedException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MultipartException;

import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.servlet.ServletException;
import javax.validation.*;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public class ExceptionControllerAdviceHelper {
    private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionControllerAdviceHelper.class);

    private static final String BINDING_ERROR_KEY = "binding.error";
    private static final String INTERNAL_ERROR_KEY = "internal.error";
    private static final String ARGUMENT_ERROR_KEY = "argument.error";
    private static final String ARGUMENT_ERROR_CONVERSION_FAILED_KEY = "argument.error.conversion.failed";
    private static final String ARGUMENT_ERROR_TYPE_MISMATCH_KEY = "argument.error.type.mismatch";
    private static final String ARGUMENT_ERROR_CONVERSION_NOT_SUPPORTED_KEY = "argument.error.conversion.not.supported";
    private static final String SERVLET_ERROR_METHOD_NOT_SUPPORTED_KEY = "servlet.error.method.not.supported";
    private static final String SERVLET_ERROR_MEDIA_TYPE_NOT_SUPPORTED_KEY = "servlet.error.media.type.not.supported";
    private static final String SERVLET_ERROR_MEDIA_TYPE_NOT_ACCEPTABLE_KEY = "servlet.error.media.type.not.acceptable";
    private static final String SYNTAX_ERROR_KEY = "syntax.error";
    private static final String SYNTAX_ERROR_JSON_PARSING_KEY = "syntax.error.json.parsing";
    private static final String SYNTAX_ERROR_WRONG_VALUE_KEY = "syntax.error.wrong.value";
    private static final String SYNTAX_ERROR_INCORRECT_INPUT_KEY = "syntax.error.incorrect.input";
    private static final String SYNTAX_ERROR_WRONG_FORMAT_KEY = "syntax.error.wrong.format";
    private static final String VALIDATION_ERROR_KEY = "validation.error";
    private static final String VALIDATION_ERROR_CONSTRAINT_VIOLATION_KEY = "validation.error.constraint.violation";
    private static final String SERVLET_BINDING_ERROR_KEY = "servlet.binding.error";
    private static final String SERVLET_BINDING_ERROR_MISSING_PATH_VARIABLE_KEY = "servlet.binding.error.missing.path.variable";
    private static final String SERVLET_BINDING_ERROR_MISSING_REQUEST_PARAM_KEY = "servlet.binding.error.missing.request.param";
    private static final String MULTIPART_RESOLUTION_ERROR_KEY = "multipart.request.resolution.error";

    private static final ConcurrentMap<String, Optional<Class<?>>> RESOLVED_CLASSES = new ConcurrentHashMap<>();

    public static ResponseEntity<ApiResponse<?>> handleThrowable(Throwable t, boolean noDescription) {
        if (instanceOf(t, "ai.noteful.commondata.common.exception.ResponseException")) {
            ResponseException re = (ResponseException) t;
            return new ResponseEntity<>(ex(noDescription, handleException(re)), toHttpStatus(re.statusCode));
        }
        if (instanceOf(t, "org.springframework.http.converter.HttpMessageNotReadableException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((HttpMessageNotReadableException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "org.springframework.core.convert.ConversionFailedException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((ConversionFailedException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "org.springframework.validation.BindException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((BindException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "org.springframework.web.multipart.MultipartException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((MultipartException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "org.springframework.web.bind.MissingPathVariableException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((MissingPathVariableException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "org.springframework.web.bind.MissingServletRequestParameterException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((MissingServletRequestParameterException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "org.springframework.web.bind.ServletRequestBindingException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((ServletRequestBindingException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "org.springframework.web.bind.MethodArgumentNotValidException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((MethodArgumentNotValidException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "org.springframework.web.method.annotation.MethodArgumentTypeMismatchException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((MethodArgumentTypeMismatchException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "org.springframework.web.method.annotation.MethodArgumentConversionNotSupportedException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((MethodArgumentConversionNotSupportedException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "org.springframework.web.HttpRequestMethodNotSupportedException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((HttpRequestMethodNotSupportedException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "org.springframework.web.HttpMediaTypeNotSupportedException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((HttpMediaTypeNotSupportedException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "org.springframework.web.HttpMediaTypeNotAcceptableException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((HttpMediaTypeNotAcceptableException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "javax.validation.ConstraintViolationException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((ConstraintViolationException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "javax.validation.ValidationException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((ValidationException) t)), HttpStatus.BAD_REQUEST);
        }
        if (instanceOf(t, "javax.servlet.ServletException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((ServletException) t)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (instanceOf(t, "org.springframework.dao.DataAccessException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((DataAccessException) t)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (instanceOf(t, "java.sql.SQLException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((SQLException) t)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (instanceOf(t, "com.freesolutions.pocket.pocketrestexchange.exception.RestExchangeException")) {
            return new ResponseEntity<>(ex(noDescription, handleException((RestExchangeException) t)), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        if (instanceOf(t, "org.apache.catalina.connector.ClientAbortException")) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);//no body, ok
        }
        return new ResponseEntity<>(ex(noDescription, handleException(t)), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private static ApiResponse<?> handleException(ResponseException e) {
        if (toHttpStatus(e.statusCode).is5xxServerError()) {
            LOGGER.error("", e);
        } else {
            LOGGER.debug("", e);
        }
        return e.response != null ?
            e.response :
            ApiResponse.failure(e, e.reason, null);
    }

    private static ApiResponse<?> handleException(HttpMessageNotReadableException e) {
        LOGGER.debug("", e);

        //detect key / caption / message
        KeyAware kaMessage = KeyAware.of(e.getMessage());
        String caption = "Syntax error";
        String key = kaMessage.keyOrDefault(SYNTAX_ERROR_KEY);
        String message = (kaMessage.key != null && kaMessage.value != null) ? ": " + kaMessage.value : null;
        if (e.getCause() instanceof JsonParseException) {
            caption = "Syntax error: json parsing";
            key = kaMessage.keyOrDefault(SYNTAX_ERROR_JSON_PARSING_KEY);
        } else if (e.getCause() instanceof InvalidFormatException) {
            caption = "Syntax error: incorrect value";
            key = kaMessage.keyOrDefault(SYNTAX_ERROR_WRONG_VALUE_KEY);
        } else if (e.getCause() instanceof MismatchedInputException) {
            caption = "Syntax error: incorrect input";
            key = kaMessage.keyOrDefault(SYNTAX_ERROR_INCORRECT_INPUT_KEY);
        } else if (e.getCause() instanceof JsonMappingException) {
            caption = "Syntax error: wrong format";
            key = kaMessage.keyOrDefault(SYNTAX_ERROR_WRONG_FORMAT_KEY);
        }

        //location
        String location = null;
        if (e.getCause() instanceof JsonProcessingException) {
            location = extractLocation((JsonProcessingException) e.getCause());
        }

        //reference
        String reference = null;
        if (e.getCause() instanceof JsonMappingException) {
            reference = extractReference((JsonMappingException) e.getCause());
        }

        return ApiResponse.failure(
            Reason.of(key, reference),
            caption +
                (location != null ? ", location: " + location : "") +
                (reference != null ? ", field: " + reference : "") +
                (message != null ? message : "")
        );
    }

    private static ApiResponse<?> handleException(ConversionFailedException e) {
        LOGGER.debug("", e);
        return ApiResponse.failure(
            e,
            Reason.of(ARGUMENT_ERROR_CONVERSION_FAILED_KEY),
            "Argument error: "
        );
    }

    private static ApiResponse<?> handleException(BindException e) {
        LOGGER.debug("", e);
        List<Reason> reasons = new ArrayList<>();
        StringBuilder b = new StringBuilder("(");
        boolean first = true;
        for (ObjectError objectError : e.getBindingResult().getAllErrors()) {
            String reference = extractReference(objectError);
            KeyAware kaMessage = KeyAware.of(objectError.getDefaultMessage());
            if (!first) {
                b.append("; ");
            } else {
                first = false;
            }
            reasons.add(Reason.of(kaMessage.keyOrDefault(BINDING_ERROR_KEY), reference));
            b.append("field: ").append(reference).append(" -> ").append(kaMessage.value);
        }
        b.append(")");
        return ApiResponse.failure(reasons, "Binding errors: " + b);
    }

    private static ApiResponse<?> handleException(MultipartException e) {
        LOGGER.debug("", e);
        return ApiResponse.failure(
            e,
            Reason.of(MULTIPART_RESOLUTION_ERROR_KEY),
            "Multipart query error: "
        );
    }

    private static ApiResponse<?> handleException(MissingPathVariableException e) {
        LOGGER.debug("", e);
        return ApiResponse.failure(
            e,
            Reason.of(SERVLET_BINDING_ERROR_MISSING_PATH_VARIABLE_KEY, e.getVariableName()),
            "Servlet binding error: "
        );
    }

    private static ApiResponse<?> handleException(MissingServletRequestParameterException e) {
        LOGGER.debug("", e);
        return ApiResponse.failure(
            e,
            Reason.of(SERVLET_BINDING_ERROR_MISSING_REQUEST_PARAM_KEY, e.getParameterName()),
            "Servlet binding error: "
        );
    }

    private static ApiResponse<?> handleException(ServletRequestBindingException e) {
        LOGGER.debug("", e);
        return ApiResponse.failure(
            e,
            Reason.of(SERVLET_BINDING_ERROR_KEY),
            "Servlet binding error: "
        );
    }

    private static ApiResponse<?> handleException(MethodArgumentNotValidException e) {
        LOGGER.debug("", e);
        List<Reason> reasons = new ArrayList<>();
        StringBuilder b = new StringBuilder("(");
        boolean first = true;
        for (ObjectError objectError : e.getBindingResult().getAllErrors()) {
            String reference = extractReference(objectError);
            KeyAware kaMessage = KeyAware.of(objectError.getDefaultMessage());
            if (!first) {
                b.append("; ");
            } else {
                first = false;
            }
            reasons.add(Reason.of(kaMessage.keyOrDefault(ARGUMENT_ERROR_KEY), reference));
            b.append("field: ").append(reference).append(" -> ").append(kaMessage.value);
        }
        b.append(")");
        return ApiResponse.failure(reasons, "Argument errors: " + b);
    }

    private static ApiResponse<?> handleException(MethodArgumentTypeMismatchException e) {
        LOGGER.debug("", e);
        return ApiResponse.failure(
            e,
            Reason.of(ARGUMENT_ERROR_TYPE_MISMATCH_KEY, e.getName()),
            "Argument error: "
        );
    }

    private static ApiResponse<?> handleException(MethodArgumentConversionNotSupportedException e) {
        LOGGER.debug("", e);
        return ApiResponse.failure(
            e,
            Reason.of(ARGUMENT_ERROR_CONVERSION_NOT_SUPPORTED_KEY, e.getName()),
            "Argument error: "
        );
    }

    private static ApiResponse<?> handleException(HttpRequestMethodNotSupportedException e) {
        LOGGER.debug("", e);
        return ApiResponse.failure(
            e,
            Reason.of(SERVLET_ERROR_METHOD_NOT_SUPPORTED_KEY),
            "Servlet error: "
        );
    }

    private static ApiResponse<?> handleException(HttpMediaTypeNotSupportedException e) {
        LOGGER.debug("", e);
        return ApiResponse.failure(
            e,
            Reason.of(SERVLET_ERROR_MEDIA_TYPE_NOT_SUPPORTED_KEY),
            "Servlet error: "
        );
    }

    private static ApiResponse<?> handleException(HttpMediaTypeNotAcceptableException e) {
        LOGGER.debug("", e);
        return ApiResponse.failure(
            e,
            Reason.of(SERVLET_ERROR_MEDIA_TYPE_NOT_ACCEPTABLE_KEY),
            "Servlet error: "
        );
    }

    private static ApiResponse<?> handleException(ConstraintViolationException e) {
        LOGGER.debug("", e);
        List<Reason> reasons = new ArrayList<>();
        StringBuilder b = new StringBuilder("(");
        boolean first = true;
        for (ConstraintViolation<?> violation : toOrdered(e.getConstraintViolations())) {
            String reference = extractReference(violation);
            KeyAware kaMessage = KeyAware.of(violation.getMessage());
            if (!first) {
                b.append("; ");
            } else {
                first = false;
            }
            reasons.add(Reason.of(kaMessage.keyOrDefault(VALIDATION_ERROR_CONSTRAINT_VIOLATION_KEY), reference));
            b.append("field: ").append(reference).append(" -> ").append(kaMessage.value);
        }
        b.append(")");
        return ApiResponse.failure(reasons, "Constraint violations: " + b);
    }

    private static ApiResponse<?> handleException(ValidationException e) {
        LOGGER.debug("", e);
        return ApiResponse.failure(e, Reason.of(VALIDATION_ERROR_KEY), "Validation error: ");
    }

    private static ApiResponse<?> handleException(ServletException e) {
        LOGGER.error("", e);
        return ApiResponse.failure(e, Reason.of(INTERNAL_ERROR_KEY), "Servlet error: ");
    }

    private static ApiResponse<?> handleException(DataAccessException e) {
        LOGGER.error("", e);
        return ApiResponse.failure(e, Reason.of(INTERNAL_ERROR_KEY), "Data access error: ");
    }

    private static ApiResponse<?> handleException(SQLException e) {
        LOGGER.error("", e);
        return ApiResponse.failure(e, Reason.of(INTERNAL_ERROR_KEY), "SQL error: ");
    }

    private static ApiResponse<?> handleException(RestExchangeException e) {
        LOGGER.error("", e);
        return ApiResponse.failure(e, Reason.of(INTERNAL_ERROR_KEY), "REST error: ");
    }

    private static ApiResponse<?> handleException(Throwable t) {
        LOGGER.error("", t);
        return ApiResponse.failure(t, Reason.of(INTERNAL_ERROR_KEY), "Unknown error: " + t.getClass().getName() + " -> ");
    }

    @Nullable
    private static String extractLocation(JsonProcessingException e) {
        JsonLocation location = e.getLocation();
        return location != null ? ("(row: " + location.getLineNr() + ", column: " + location.getColumnNr() + ")") : null;
    }

    @Nullable
    private static String extractReference(JsonMappingException e) {
        StringBuilder b = new StringBuilder();
        for (JsonMappingException.Reference reference : e.getPath()) {

            //check for simple field
            if (reference.getFieldName() != null) {
                if (b.length() > 0) {
                    b.append('.');
                }
                b.append(reference.getFieldName());
                continue;
            }

            //check for array element
            if (reference.getIndex() >= 0) {
                //NOTE: no dot
                b.append('[').append(reference.getIndex()).append(']');
                continue;
            }

            //default
            if (b.length() > 0) {
                b.append('.');
            }
            b.append("<?>");
        }
        return b.length() > 0 ? b.toString() : null;
    }

    @Nullable
    private static String extractReference(ObjectError objectError) {

        //default case:
        //1) search second dot in first code, reference is remaining part
        //2) if no second dot present, then no fields available to be ref
        String[] codes = objectError.getCodes();
        if (codes != null && codes.length > 0) {
            String code = codes[0];
            int idx = code.indexOf('.');
            if (idx >= 0) {
                idx = code.indexOf('.', idx + 1);
                if (idx >= 0) {
                    return code.substring(idx + 1);
                }
            }
        }
        return null;
    }

    @Nullable
    private static String extractReference(ConstraintViolation<?> violation) {
        StringBuilder b = new StringBuilder();
        for (Path.Node node : violation.getPropertyPath()) {
            ElementKind kind = node.getKind();

            //some kinds of elements we meet indicate that path is not lead to proper ref as we need
            switch (kind) {
                case METHOD:
                case CONSTRUCTOR:
                case PARAMETER:
                case CROSS_PARAMETER:
                case RETURN_VALUE:
                    return null;
            }

            //add index/key element
            if (node.isInIterable()) {
                //NOTE: no dot
                b.append('[');
                Integer idx = node.getIndex();
                if (idx != null) {
                    b.append(idx);
                } else {
                    Object key = node.getKey();
                    if (key != null) {
                        if (key instanceof Number || key instanceof String) {
                            b.append(key);
                        } else {
                            b.append("<?>");
                        }
                    } else {
                        b.append("<?>");
                    }
                }
                b.append(']');
            }

            //name
            if (node.getName() != null) {
                if (b.length() > 0) {
                    b.append('.');
                }
                b.append(node.getName());
            }
        }
        return b.length() > 0 ? b.toString() : null;
    }

    private static SortedSet<ConstraintViolation<?>> toOrdered(Set<ConstraintViolation<?>> unorderedViolations) {
        SortedSet<ConstraintViolation<?>> result = new TreeSet<>(
            (l, r) -> String.CASE_INSENSITIVE_ORDER.compare(
                l.getPropertyPath().toString(),
                r.getPropertyPath().toString()
            )
        );
        result.addAll(unorderedViolations);
        return result;
    }

    private static <T> ApiResponse<T> ex(boolean noDescription, ApiResponse<T> response) {
        return noDescription ? response.noDescription() : response;
    }

    private static HttpStatus toHttpStatus(@Nullable Object statusCode) {
        if (statusCode == null) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        if (statusCode instanceof HttpStatus) {
            return (HttpStatus) statusCode;
        }
        if (statusCode instanceof Integer) {
            HttpStatus httpStatus = HttpStatus.resolve((Integer) statusCode);
            return httpStatus != null ? httpStatus : HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;//default
    }

    private static boolean instanceOf(Object obj, String fullClassName) {
        Optional<Class<?>> cls = RESOLVED_CLASSES.get(fullClassName);
        if (cls == null) {
            synchronized (RESOLVED_CLASSES) {
                cls = RESOLVED_CLASSES.get(fullClassName);
                if (cls == null) {
                    try {
                        cls = Optional.of(Class.forName(fullClassName));
                    } catch (ClassNotFoundException e) {
                        RESOLVED_CLASSES.put(fullClassName, Optional.empty());
                        return false;
                    }
                    RESOLVED_CLASSES.put(fullClassName, cls);
                }
            }
        }
        return cls.isPresent() && cls.get().isInstance(obj);
    }
}
