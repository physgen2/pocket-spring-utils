package com.freesolutions.pocket.pocketspringutils.lifecycle;

import com.freesolutions.pocket.pocketlifecycle.startable.Startable;
import org.slf4j.LoggerFactory;
import org.springframework.context.SmartLifecycle;

import javax.annotation.ParametersAreNonnullByDefault;

/**
 * @author Stanislau Mirzayeu
 */
@ParametersAreNonnullByDefault
public abstract class BeanLifecycle implements SmartLifecycle {

    public static final int MIN_PHASE = Integer.MIN_VALUE;
    public static final int EARLY_PHASE = Integer.MIN_VALUE + 100;
    public static final int DEFAULT_PHASE = 0;
    public static final int LATE_PHASE = Integer.MAX_VALUE - 100;
    public static final int MAX_PHASE = Integer.MAX_VALUE;

    private volatile boolean running;

    public static BeanLifecycle of(Startable instance) {
        return of(instance, DEFAULT_PHASE);
    }

    public static BeanLifecycle of(Startable instance, int phase) {
        return new BeanLifecycle() {
            @Override
            public void onStart() throws Exception {
                instance.start();
            }

            @Override
            public void onStop() {
                instance.stop();
            }

            @Override
            public int getPhase() {
                return phase;
            }
        };
    }

    @Override
    public boolean isAutoStartup() {
        return true;//we want auto-startup by default in this abstract implementation, ok
    }

    @Override
    public void start() {
        if (running) {
            return;//ignore if already running
        }
        try {
            onStart();
            this.running = true;//setup only on success
        } catch (Throwable t) {
            //always throw exceptions at start
            if (t instanceof InterruptedException) {
                Thread.currentThread().interrupt();
                throw new RuntimeException("interrupted", t);
            }
            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            }
            throw new RuntimeException(t);
        }
    }

    @Override
    public void stop() {
        //nothing, ignored according to spring docs when used <SmartLifecycle>
    }

    @Override
    public void stop(Runnable callback) {
        if (!running) {
            return;//ignore if was not running
        }
        boolean interrupted = false;
        try {
            onStop();
        } catch (Throwable t) {//don't throw exceptions at stop
            if (t instanceof InterruptedException) {//always check fact, don't trust compiler
                interrupted = true;
            }
            LoggerFactory.getLogger(getClass()).error("", t);
        } finally {
            this.running = false;
            try {
                callback.run();
            } catch (Throwable t) {//don't throw exceptions at stop
                if (t instanceof InterruptedException) {//always check fact, don't trust compiler
                    interrupted = true;
                }
                LoggerFactory.getLogger(getClass()).error("", t);
            }
            if (interrupted) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    @Override
    public int getPhase() {
        return DEFAULT_PHASE;//implementation can override this if necessary, ok
    }

    public abstract void onStart() throws Exception;

    public abstract void onStop();
}
